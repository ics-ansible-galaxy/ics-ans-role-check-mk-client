ics-ans-role-check-mk-client
===================

Ansible role to install check-mk-client.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
check_mk_client_package: #path to check mk agent(s)
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-check-mk-client
```

License
-------

BSD 2-clause
