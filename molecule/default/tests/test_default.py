import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_checkmk_agent(host):
    checkmk_agent = host.service("check_mk.socket")
    assert checkmk_agent.is_running
    assert checkmk_agent.is_enabled
